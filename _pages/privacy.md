---
layout: page
title: Privacy
permalink: /privacy/
---

## Privacy

This site makes use of [Google Analytics](http://www.google.com/analytics/); analytic cookies are used to collect information (in an anonymous form) about how visitors use this website, including the number of visitors to the site, where visitors have come to the site from and the pages they visited, which is used to help improve it.