---
layout: page
title: Get Involved
permalink: /get-involved/
---

# Get Involved
{:.centered-text}

Dekko is a 100% open source project and is developed collaboratively with people all over the world. Even if you're not a coder there are plenty ways to get involved and all contributions are welcomed no matter how big or small.
{:.centered-text}

***
{:.short}

### Contribute Code

Developers looking to contribute code to the Dekko Project can read this getting started Wiki page.

[Developer Wiki](https://gitlab.com/dekkoproject/dekko/wikis/home){:.button}

***
{:.short}


### Bug Reporting &amp; Fixing

If you use Dekko finding and reporting bugs to our issue tracker is much appreciated. But Dekko also has plenty of open bugs that need squashing, if you're willing and capable.

[Issue Tracker](https://gitlab.com/dekkoproject/dekko/issues){:.button}
[Bitesized Bugs](https://gitlab.com/dekkoproject/dekko/issues?label_name%5B%5D=bitesize){:.button}

***
{:.short}

### Write Documentation

Dekko needs to provide documentation for both users and developers. Writing new documentation or making a change to existing documentations is as simple as opening a pull request on our documentation respository.

[Documentation](https://gitlab.com/dekkoproject/documentation/user-documentation){:.button}

***
{:.short}

### Translate Dekko

To make Dekko as accessible to as many people as possible, the project needs translations. Dekko uses Weblate for translations, which you can help out with here

[Translations](https://translate.ubports.com/#list-dekko){:.button}

***
{:.page-break}
