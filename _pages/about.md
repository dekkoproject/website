---
layout: page
title: About
permalink: /about/
---

## About

The Dekko Project was started back in 2014 with the goal of creating an email client for Ubuntu Touch. It was originally targeted at mobile devices and was powered by the superb [Trojitá](http://trojita.flaska.net) IMAP library. After multiple iterations of design, development and user feedback it became apparent the current direction didn't fulfill the requirements and user expectations for a convergent email client. Step in Dekko 2.0 (well 1.0 as we never reached a 1.0 version). 

Dekko has been re-written from scratch with convergence in mind from the start, ensuring to take care of all lessons learned from previous versions. This has brought about some pretty great features that previously were not possible.

* __Multiple Accounts__ - You can add as many accounts as you need with very little overhead
* __Unified Inbox__ - You can view all your inboxes in one view or open them individually
* __Smart Folders__ - Taking on a different approach to organising your mail
* __IMAP, POP3 and SMTP support__ - POP3 is still experimental but making good progress
* __Highly Extensible__ - Dekko can be easily extended throughout via the use of official and third-party plugins
* __Contacts__ - Built in address book that supports CardDAV.

There's also many more features available you would expect of a modern email client and plenty more planned as well. Take a look at the [roadmap](https://code.dekkoproject.org/dekko-dev/dekko/wikis/roadmap) to see some of those plans.


### Credits

* [Dan Chapman](https://code.dekkoproject.org/dpniel) - Project founder and developer
* [Alan Pope](https://popey.com) - This guy's a real gent, always willing to help.
* [Canonical design team](http://design.canonical.com) - Helped with the initial convergent designs and UX. You guys rock!
* [Sam Hewitt](https://samuelhewitt.com) - Made this awesome website. Cheers!