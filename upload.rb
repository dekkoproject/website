#!/usr/bin/env ruby

require 'rubygems'
require 'net/scp'
puts "Deploying to dekkoproject.org"

Dir.chdir("./public")

Net::SCP.upload!("ssh.dekkoproject.org", "dekkoproject.org",
  ".", "/www",
  :ssh => { :password => ENV['SSHPASS'] },
  :recursive => true,
  :preserve => true,
  :verbose => true
)

puts "Deployed to dekkoproject.org"
