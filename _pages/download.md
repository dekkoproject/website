---
layout: page
title: Download
permalink: /download/
---

## Download

Dekko is still under heavy development so there aren't any official releases yet. Below are a few ways you can try
out the latest code.

### Snap Packages

An experimental package is available in the Ubuntu Snap store.

```bash
snap install ubuntu-app-platform
snap install --edge --devmode dekko
```

### Click Packages

There currently isn't an up to date click package of Dekko 2.0 available in the OpenStore. Work is currently
ongoing to make this happen. If you would like to support this effort please consider going over to our
patreon page.


### Build from source

You can also build the latest trunk from source to see the latest developments if your on Ubuntu 16.04+

```bash
git clone https://gitlab.com/dekkoproject/dekko.git && cd dekko
./setup-dev-env
./run-dekko2
```
