---
layout: post
title: "Dekko Development Update"
date: 2017-08-21 12:34:56 -0000
tags: dekko update ubports
---

I thought it was about time I got word out on the current progress of Dekko and it's plans for the future. I had hoped to get this
first update out sooner, so apologies for that.

<!-- break -->

## What's been happening?

Alot has happened over the last few months, with planning the road forward and making changes that will allow the Dekko
to be able to progress as an independent project into the future. My main focus is to get Dekko working again on Ubuntu Touch and
Ubports devices and continue with Dekko's convergence efforts as well as provide Dekko as a snap package for users to try on their desktops.

### Future proofing

First job was to rework the code to be more modular, with the goal to seperate **all** UI code from application logic, which
also includes any QML application logic that is often needed to tie into the backend. I wanted it to be possible to switch 
the entire UI without having to touch any of the core logic. This would allow Dekko to easily be able to switch UI toolkits with
minimal fuss. Especially as the current toolkit's life expectancy is still uncertain, so the prospect of just needing to re-write the UI is
not so daunting and a real time saver.

I'm pleased to say this transition has gone really well and it should now be pretty easy to switch Dekko's current UI with one based on
other QtQuick based UI frameworks. Although my plans are to follow the toolkit used by Yunit and Ubports for now, I would love to see
new contributions for other frameworks like QtQuick Controls 2 or LiriOS's Fluid (Material Design).

### Plugin support

Although I try to keep Dekko as simple and functional by default for it's users, I always wanted it to also be as 
extendable as possible and have some sort of support for plugins that add functionality per a users needs. Something similar 
to what you get with Thunderbird, where there is an assortment of third party extensions to fulfill specific needs.

There is now initial support for various types of plugins:

* __Action plugins__ - These can be used to add actions to context menus, action lists, menu entries or add new buttons in page headers
* __Item plugins__ - These are visual plugins that add or replace complete components in the UI
* __Listener plugins__ - These are non-visual and get loaded so you can listen and react to specific events that go through Dekko's event framework
* __Service plugins__ - These are background services that are managed by Dekko and can be used for scenarios like syncing data with an external service
* __Protocol plugins__ - These provide the messaging functionality. Current plugins are for IMAP, POP3 and SMTP protocols
* __ContentManager plugins__ - These manage the storage, retrieval and filtering of messages and message content.


Not _all_ areas of the UI support plugins yet, but most will do as time goes by. Also the only way to manage plugins 
right now is from a config file but a more user friendly way will appear in the near future.  

Dekko's default UI is also made up from these various plugin types and shipped as an Item plugin, which is what allows us 
to be able to add/remove/modify the UI with relative ease.


### New translations infrastructure

Dekko will now be using weblate for translations at weblate.dekkoproject.org. In the next few days I will be opening it up to translators to start preparing for the first release of this all new version of Dekko.

Once it's live I will put out an official call for translations to the Ubports community.

## What's next

First task is to get a release out to the Ubports OpenStore to start getting user feedback. I'm having issues (again!) getting Dekko to build for 15.04
so it's going to some time to get this figured out.

Once that's out the way the next cycle is going to focus on optimizing for touch devices again, and improve the UX around interactions 
with small form factor devices. It currently works pretty well with a mouse and keyboard on large form factor devices but not much testing has 
been done on small screens.

Also planning needs to be done on how best to integrate with the system contacts and calendar, as Dekko is aiming to be it's own PIM suite
and some preliminary plugins exist for it's contacts and calendar backends. A solution is needed that integrates the two seemlessly. Best thing
will be to schedule a meeting with the Ubports team and have a discussion about the way forward.


## Thanks to our supporters

Thanks to everyone supporting the project. You have really given me the push to carry on with Dekko and to try and make it the best it can be.

You folks rock!!!

If you would like to support the project then head on over to our [patreon campaign](https://patreon.com/dekkoproject)