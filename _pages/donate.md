---
layout: page
title: Donate
permalink: /donate/
---

## Donate
{:.centered-text}

Everybody can help by making a financial contribution. This finances our server infrastructure, and helps covering additional costs. All contributors work for for free in their spare time, but with your help we will be able to finance a full-time developer (or maybe more) soon!. We have various ways you can provide financial support
{:.centered-text}

***
{:.short}

### Patreon
{:.centered-text}

If you wish to support us with monthly contributions then this is the one for you. Our patreon campaign provides multiple levels of rewards.
{:.centered-text}


[Patreon](https://patreon.com/dekkoproject){:.button}
{:.centered-text}


***
{:.short}

### PayPal
{:.centered-text}

Or if a one off payment is more your thing, then we have you covered there as well.
{:.centered-text}

[£5](https://paypal.me/Dekko/5){:.button}
[£10](https://paypal.me/Dekko/10){:.button}
[£20](https://paypal.me/Dekko/20){:.button}
[Custom](https://paypal.me/Dekko){:.button}
{:.centered-text}
